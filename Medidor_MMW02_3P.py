from MMW02 import MMW02
import time
import ApiFeeder
from Thread import Thread
from requests.exceptions import ConnectionError
import pprint
import serial
import Db
import json

_TEMPO_MAXIMO_ATUALIZACAO = 10
_GRANDEZAS = ['v_a', 'v_b', 'v_c', 'i_a', 'i_b', 'i_c', 'fpt', 'p_w', 'p_var', 'p_va', 'f', 'e_kwh', 'e_kvarh', 'e_kvah']
#_GRANDEZAS = ['v_a', 'v_b', 'v_c', 'fpt', 'p_w', 'p_var', 'p_va', 'f', 'e_kwh', 'e_kvarh', 'e_kvah']

class Mm_02_3p_medidor(MMW02):
    def __init__(self, id, name, modbus_id, activated, porta, boundrate):
        super(Mm_02_3p_medidor, self).__init__(porta, boundrate, modbus_id)
        self.id = id
        self.name = name
        self.modbus_id = modbus_id
        self.activated = bool(activated)
        self.porta = porta
        self.boundrate = boundrate
    
    def printDados(self, dados):
        print ("medidor " + self.name + " - " + str(self.modbus_id))
        for grandeza in _GRANDEZAS:
            print (grandeza + ": " + str(dados[grandeza]))
            
    def lerGrandezas(self):
        dados = {}
        self.conectar()
        for grandeza in _GRANDEZAS:
            dados[grandeza] = self.ler(grandeza)
        dados['mm02_3p_measurer_id'] = self.id
        dados['timestamp'] = time.strftime('%Y-%m-%d %H:%M:%S')
        self.desconectar()
        time.sleep(1)
        #self.printDados(dados)
        return dados


class Mm_02_3p(object):
    db = None
    def __init__(self, apiUrl, feedingtoken):
        self.feeder = ApiFeeder.ApiFeeder(apiUrl, feedingtoken)
        self.thread_verificaAtualizacoes = Thread(self.verificarAtualizacoes)
        self.redefinir()
        
    def redefinir(self):
        self.emErro = False
        self.erro = None
        self.info = self.feeder.updateInfo()
        self.hasUpdate = False
        self.contador = 0
        if(int(self.info['type_of_measurer']) != 1):
            raise Exception("Erro: confito de tipo de medidor. Verifique se o feedingtoken faz parte deste medidor.")
        self.iniciar()
        
    def iniciar(self):
        self.serial_port = "/dev/ttyUSB0" #self.info['mm02_3p_settings']['serial_port']
        self.boundrate = int(self.info['mm02_3p_settings']['bound_rate'])
        self.activated =  bool(self.info['activated'])
        self.delay = self.info['mm02_3p_settings']['delay']

        self.medidores = []
        for measurer in self.info['mm02_3p_measurers']:
            if bool(measurer['activated']):
                self.medidores.append(Mm_02_3p_medidor(measurer['id'], measurer['name'], measurer['modbus_id'], measurer['activated'], self.serial_port, self.boundrate))

        deveAtivarThreadAtualizacoes = not self.activated or self.delay > _TEMPO_MAXIMO_ATUALIZACAO or len(self.medidores) == 0
        self.setarAtividadeThreadAtualizacoes(deveAtivarThreadAtualizacoes)
        if self.db is None:
            try:
                self.db = Db.DataBase(self.info['type_of_measurer'])
            except Exception:
                print('Erro ao conectar com o banco de dados local')
                exit()
        
            
    
    def lerTodos(self):
        medidorAtual = None
        try:
            print('Iniciando medicoes')
            dados = []
            for medidor in self.medidores:
                if medidor.activated:
                    medidorAtual = medidor
                    dados.append(medidor.lerGrandezas())
                    print('   Medidor "' + medidor.name + '"  OK')
            print("Dados Lidos")
            return dados
        except serial.serialutil.SerialException as ex:
            print("********************************")
            print(ex)
            print("********************************")
            self.ativarModoErro('Verifique a configuracao serial/modbus')
            return None
        except Exception as ex:
            print("********************************")
            print(ex)
            print("********************************")
            self.ativarModoErro('Verifique a configuracao serial/modbus (medidor:' + medidorAtual.name + '(' + str(medidorAtual.modbus_id) + ')')
            return None

    def ativarModoErro(self, erro):
        self.erro = erro
        self.emErro = True
        self.setarAtividadeThreadAtualizacoes(True)
        self.feeder.setError(erro)
        print('!!! ESTACAO EM ERRO: ' + self.erro + ' !!!')

    def verificarAtualizacoes(self):
        self.hasUpdate = self.feeder.hasUpdate()
        print('**ROTINA DE ATUALIZACAO**: ' + ('Atualizacoes Disponiveis' if self.hasUpdate else 'Sem Atualizações'))
        if(self.activated):
            time.sleep(_TEMPO_MAXIMO_ATUALIZACAO)
        else:
            time.sleep(10)
    
    def setarAtividadeThreadAtualizacoes(self, atividade):
        if atividade:
            if self.thread_verificaAtualizacoes.isAlive():
                print('resumindo rotina de atualizacao')
                self.thread_verificaAtualizacoes.resume()
            else:
                print('iniciando rotina de atualizacao')
                self.thread_verificaAtualizacoes.start()
        elif self.thread_verificaAtualizacoes.isAlive():
            print('cessando rotina de atualizacao')
            self.thread_verificaAtualizacoes.pause()


    def rotinaDeSinconizacao(self):
        print(' Iniciando rotina de Sincronizacao')
        while self.db.hasData:
            ids, dados = self.db.getData()
            self.feeder.feed(dados)
            self.db.deleteRows(ids)
            print(' Sincronizados: ', ids)
        print('Dados sincronizados com sucesso')
        
    def main_loop(self):
        if self.hasUpdate:
            self.redefinir()
        elif self.activated and len(self.medidores) > 0:
            if self.contador >= self.delay:
                #pprint.pprint(self.info)
                dados = self.lerTodos()
                self.contador = 0
                if dados is not None:
                    try:
                        self.feeder.feed(dados)
                        if self.db.hasData:
                            self.rotinaDeSinconizacao()
                    except ApiFeeder.FeedingException as ex:
                        print(str(ex))
                        if(ex.code == 'update' or ex.code == 'activity'):
                            self.hasUpdate = True
                        elif (ex.code == "connection" or ex.code == "other"):
                            print('Erro de conexão. Salvando dados localmente.')
                            self.db.save(dados)
                if self.emErro:
                    self.redefinir()
            else:
                print('Delay: ' + str(self.delay) + '.    Tempo restante: ' + str(int(self.delay) - self.contador) + 's')
                self.contador += 1
        elif len(self.medidores) == 0:
            print('Medidores desativados')
        else:
            print("Estacao desativada")
        time.sleep(1)
