import threading
import time

class Thread (threading.Thread):
    
    def __init__(self, function):
        threading.Thread.__init__(self)
        self.stopper = threading.Event()
        self.pauser = threading.Event()
        self.function = function

    def run(self):
      while not self.stopper.is_set():
            if not self.pauser.is_set():
                self.function()
            else:
                time.sleep(1)

    def stop(self):
        self.stopper.set()
    
    def pause(self):
        self.pauser.set()
    
    def resume(self):
        self.pauser.clear()


"""
#testes

class Foo(object):
    def __init__(self):
        self.c = 1
    def foo(self):
        print(self.c)
        self.c += 1
        time.sleep(1)



t = Thread(Foo().foo)
t.start()
time.sleep(5)
t.pause()
time.sleep(3)
t.resume()
time.sleep(2)
t.stop()
"""