import time
import Db
import random

urlApi = 'http://localhost/dave/API/salvar'
grandezas = ['v_a', 'v_b', 'v_c', 'fpt', 'p_w', 'p_var', 'p_va', 'f', 'e_kwh', 'e_kvarh', 'e_kvah']


dados = {}
while True:
    dados['v_a'] = random.random()*30 + 200
    dados['v_b'] = random.random()*30 + 200
    dados['v_c'] = random.random()*30 + 200
    dados['fpt'] = random.random()*0.1 + 0.9
    dados['p_w'] = random.random()*5 + 45
    dados['p_var'] = random.random()*5 + 10
    dados['p_va'] = random.random()*5 + 50
    dados['f'] = random.random()*2 + 59
    dados['e_kwh'] = random.random() + 5
    dados['e_kvarh'] = random.random() + 5
    dados['e_kvah'] = random.random() + 5

    print('Dados lidos: ', dados)
    print('Salvando...')
    Db.salvarDados(urlApi, dados)    
    time.sleep(5)

