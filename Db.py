import json, requests, time
import sqlite3

class DataBase(object):

    def __init__(self, type_of_measurer):
        self.connection = sqlite3.connect('dadoslocais.db')
        cursor = self.connection.cursor()
        cursor.execute("""
                    CREATE TABLE IF NOT EXISTS data (
                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        mm02_3p_measurer_id INTEGER,
                        timestamp TEXT NOT NULL,
                        v_a REAL NULL,
                        v_b REAL NULL,
                        v_c REAL NULL,
                        i_a REAL NULL,
                        i_b REAL NULL,
                        i_c REAL NULL,
                        fpt REAL NULL,
                        p_w REAL NULL,
                        p_var REAL NULL,
                        p_va REAL NULL,
                        f REAL NULL,
                        e_kwh REAL NULL,
                        e_kvarh REAL NULL,
                        e_kvah REAL NULL);""")
        self.connection.close()
        self.hasData = False
        self.verifyLocalData()

    def connect(self):
        self.connection = sqlite3.connect('dadoslocais.db')
    
    def desconnect(self):
        self.connection.close()

    def save(self, data):
        tuples = []
        for row in data:
            tuples.append((
                row['mm02_3p_measurer_id'],
                row['timestamp'],
                row['v_a'],
                row['v_b'],
                row['v_c'],
                row['i_a'],
                row['i_b'],
                row['i_c'],
                row['fpt'],
                row['p_w'],
                row['p_var'],
                row['p_va'],
                row['f'],
                row['e_kwh'],
                row['e_kvarh'],
                row['e_kvah']))
        self.connect()
        cursor = self.connection.cursor()
        cursor.executemany("""INSERT INTO data (
                                'mm02_3p_measurer_id',
                                'timestamp',
                                'v_a',
                                'v_b',
                                'v_c',
                                'i_a',
                                'i_b',
                                'i_c',
                                'fpt',
                                'p_w',
                                'p_var',
                                'p_va',
                                'f',
                                'e_kwh',
                                'e_kvarh',
                                'e_kvah') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""", tuples)
        self.connection.commit()
        self.desconnect()
        self.hasData = True
    
    def verifyLocalData(self):
        self.connect()
        cursor = self.connection.cursor()
        cursor.execute("SELECT COUNT(id) FROM data")
        self.hasData = cursor.fetchone()[0] > 0
        self.desconnect()
    
    def getData(self):
        dados = []
        ids = []
        self.connect()
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM data LIMIT 10")
        for row in cursor.fetchall():
            print (row)
            dados.append({
                'mm02_3p_measurer_id': row[1],
                'timestamp': row[2],
                'v_a': row[3],
                'v_b': row[4],
                'v_c': row[5],
                'i_a': row[6],
                'i_b': row[7],
                'i_c': row[8],
                'fpt': row[9],
                'p_w': row[10],
                'p_var': row[11],
                'p_va': row[12],
                'f': row[13],
                'e_kwh': row[14],
                'e_kvarh': row[15],
                'e_kvah': row[16] 
            })
            ids.append(row[0])
        self.desconnect()
        return ids, dados
    
    def deleteRows(self, ids):
        query = 'DELETE FROM data WHERE id IN (%s)' % ','.join('?' for i in ids)
        #print(query)
        self.connect()
        cursor = self.connection.cursor()
        cursor.execute(query, [str(id) for id in ids])
        self.connection.commit()
        self.desconnect()
        self.verifyLocalData()

"""
db = DataBase(1)
db.verifyLocalData()
ids, dados = db.getData()
print( ids)
print (dados)
db.deleteRows(ids)
print(db.getData())
"""