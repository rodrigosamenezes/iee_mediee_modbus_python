import requests
import pprint
import json

class FeedingException(Exception):
    
    def __init__(self, message, code):
        super(FeedingException, self).__init__(message)
        self.code = code

class ApiFeeder(object):
    _info = None

    def __init__(self, url, feedingtoken):
        self._url = url
        self._headers = {'feedingToken': feedingtoken}
        self.updateInfo()

    def updateInfo(self):
        try:
            response = requests.get(self._url, headers=self._headers)
            js = response.json()
        except requests.exceptions.ConnectionError:
            print('Erro de conexão')
        except json.decoder.JSONDecodeError:
            print('Erro de conversao JSON')
            print(response.text)
        else:
            if js['ok']:
                self._info = js['data']
                return self._info
            else:
                raise Exception('Error: ' + js['message'])
    
    def hasUpdate(self):
        try:
            response = requests.get(self._url + '/has_update', headers=self._headers)
            js = response.json()
        except requests.exceptions.ConnectionError:
            print('Erro de conexão. Update necessário.')
        except json.decoder.JSONDecodeError:
            print('Erro de conversao JSON')
            print(response.text)
        else:
            if js['ok']:
                hasUpdate = bool(js['data'])
                #print(hasUpdate)
                return hasUpdate
            else:
                raise Exception('Error:' + js['message'])

    def feed(self, data):
        #pprint.pprint(data)
        #print('')
        #print(json.dumps(data))
        try:
            response = requests.post(self._url, json=data, headers=self._headers)
            js = response.json()
        except requests.exceptions.ConnectionError:
            print('Erro de conexão.')
            raise FeedingException("Erro: falha na conexao com o servidor","connection")
        except json.decoder.JSONDecodeError:
            print('Erro de conversao JSON')
            print(response.text)
            raise FeedingException("Erro: falha na conversao json","connection")
        except Exception as ex:
            print (ex)
            raise FeedingException("Erro: " + ex.message,"other")
        else:
            if js['ok']:
                print('Dados enviados')
            else:
                raise FeedingException("Erro: " + js['message'], js['code'])
    
    def setError(self, error):
        data = { 'error': error }
        try:
            response = requests.post(self._url + '/seterror', json=data, headers=self._headers)
            js = response.json()
            print(js)
        except requests.exceptions.ConnectionError:
            print('Erro de conexão. Update necessário.')
        except json.decoder.JSONDecodeError:
            print('Erro de conversao JSON')
            print(response.text)
        else:
            if js['ok']:
                print('Erro Registrado')
            else:
                raise FeedingException("Erro: " + js['message'], js['code'])


    

