from Modbus import *
import math

_ENDERECOS = {
    'v_a': 4, #tensao na fase A
    'v_b': 6, #tensao na fase B
    'v_c': 8, #tensao na fase C
    'i_a': 12, #corrente na fase A
    'i_b': 14, #corrente na fase B
    'i_c': 16, #corrente na fase C
    'fpt': 26, #fator de potencia total
    'p_w': 42, #potencia ativa total
    'p_var': 50, #potencia reativa total
    'p_va': 58, #potencia aparente total
    'f': 66, #frequencia instantanea
    'e_kwh': 302, #energia ativa total bruta
    'e_kvarh': 310, #energia reativa total bruta
    'e_kvah': 324, #energia aparente total
}

class MMW02(object):

    def __init__(self, porta, boundrate, id):
        self.modbusClient = ModbusClient(porta)
        self.modbusClient.Parity = Parity.none
        self.modbusClient.UnitIdentifier = id
        self.modbusClient.Baudrate = boundrate#19200
        self.modbusClient.Stopbits = Stopbits.one
        self.id = id

    def conectar(self):
        self.modbusClient.Connect()
    
    def desconectar(self):
        self.modbusClient.close()

    def ler(self, grandeza):
        #print("idModbus: ", str(self.id), "; grandeza: ", grandeza)
        valor = self._lerFloat(_ENDERECOS[grandeza])
        #print(grandeza, ': ', valor, math.isnan(valor))
        if math.isinf(valor) or math.isnan(valor):
            valor = 0
            #print("novo Valor:", valor)
        return valor

    def _lerFloat(self, registrador):
        listaBytes = self.modbusClient.ReadInputRegisters(registrador, 2)
        #print(listaBytes)
        stringBin = self.binary(listaBytes[0]).replace('0b', '') +self.binary(listaBytes[1]).replace('0b', '')
        #print(bin(listaBytes[0]), bin(listaBytes[1]))
        #print(stringBin, len(stringBin))
        i = int(stringBin, 2)
        #print("i: ", i)
        return struct.unpack('f', struct.pack('I', i))[0]  
    
    def binary(self, num, length=16):
        return format(num, '#0{}b'.format(length + 2))
